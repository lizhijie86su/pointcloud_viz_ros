#!/usr/bin/env python
# coding: utf-8

import socket
import cv2
import numpy as np
import threading
def ProcessImageRGB (im_to_show, im_array):
    mask = [0X1F, 0X7E0, 0XF800]
    shift = [3, 3, 8]
    shift2 = [2,9,13]
    for i in range(3):
        im = (im_array & mask[i])
        if (i == 0):
            im_to_show[:,:,i] = (im << shift[i]) |  (im >> shift2[i])
        else:
            im_to_show[:,:,i] = (im >> shift[i]) |  (im >> shift2[i])
        
def ShowImage(im_type, IM_X, IM_Y ):
    global im_array
    global SecondFrame
    global DataReady
    global framecnt
    im_cnt = 0
    if (im_type == 2):
        im_to_show = np.zeros((IM_Y, IM_X, 3), np.uint8)

        win_name = "FPGA video - " + str(IM_X) + "x" + str(IM_Y) + " RGB"
    elif (im_type == 1):
        im_to_show = np.zeros((IM_Y, IM_X), np.uint8)
        win_name = "FPGA video - " + str(IM_X) + "x" + str(IM_Y) + " grayscale"
    cv2.imshow(win_name, im_to_show)
    while (True):

        if (DataReady.isSet()): 
            im_to_show = im_array
            DataReady.clear()     

        cv2.imshow(win_name, im_to_show )
        cv2.setWindowTitle(win_name, str(framecnt))
        key = cv2.waitKey(5)

        if key == ord('s'):
            filename = "Img_" + str(im_cnt)+".png"
            im_to_save = np.array(im_to_show).copy()
            cv2.imwrite(filename,im_to_save)
            im_cnt += 1
            print ("Saved image " + filename)

        if key == ord('q'):
            break

        if key == ord('r'): # send soft reset in future
            break

def main():

    global im_array
    global SecondFrame
    global DataReady
    global framecnt
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM )
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 65536*8192)
    sock.setblocking(True)
    port = 5000
    ip = '192.168.1.10'
    DataReady = threading.Event()
    sock.bind((ip,port)) 


    line_cnt = 0
    line_cnt_ts = 0
    SecondFrame = False
    framecnt = 0

    IM_X = 1280
    IM_Y = 1024
    im_type = 1 

    ShowImageThread = threading.Thread(target=ShowImage, args = (im_type, IM_X, IM_Y))
    ShowImageThread.daemon = True
    ShowImageThread.start()
    assem_flag = 0
    im_array = np.zeros((IM_Y,IM_X),np.uint8)
    while(True):
        data = sock.recv(1288)
   #     print(data)
       # PAC_PER_FRAME = data[5] + (data[6] << 8)
       # PACK_PER_LINE = int(PAC_PER_FRAME / IM_Y)
       # seg_len = int(IM_X / PACK_PER_LINE)
       # PAC_CNT = (data[7] + (data[8] << 8))
        # print("PAC_CNT=%d"%PAC_CNT)
        #line_cnt = int(PAC_CNT / PACK_PER_LINE)
        line_cnt_ts = data[5] + (data[4] << 8)
        if ((line_cnt_ts != (line_cnt + 1))&(line_cnt != 1023)):
            print("eor= %d "%line_cnt)
        line_cnt = line_cnt_ts


        if True:
      #      seg_pointer = int(offset*seg_len)
            if line_cnt<IM_Y-1:
                im_array[line_cnt,0:1279] = np.frombuffer(data[8:1287], dtype=np.uint8)
            if line_cnt == IM_Y-1:
                DataReady.set()
       

        # print(line_cnt)
        if (line_cnt == IM_Y - 1):
            cv2.imwrite("sc%d.png"%framecnt,im_array)
            framecnt = framecnt+1
            if int(framecnt/5)*5==framecnt:
                assem_flag=1
            else:
                assem_flag=0
        
            
        if (not ShowImageThread.is_alive()):
            break
       

    print("The client is quitting.")
    sock.close()
    cv2.destroyAllWindows()
    
if __name__ == '__main__':
    main()
