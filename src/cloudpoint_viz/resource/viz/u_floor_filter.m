function img_out = u_floor_filter(img,winsize,thd )
%UNTITLED2 此处显示有关此函数的摘要
%   此处显示详细说明
[height, width] = size(img);
img_out =  zeros(height, width);

for i = 1:height
    for j = 1:width
        % 计算当前像素及其相邻像素的局部方差
        row_indices = max(1, i - floor(winsize/2)):min(height, i + floor(winsize/2));
        col_indices = max(1, j - floor(winsize/2)):min(width, j + floor(winsize/2));
        local_var = var(img(row_indices, col_indices), 0, 'all');
        
        % 根据局部方差进行滤波
        if local_var < thd
            img_out(i, j) = img(i, j);
        end
    end
end


end

