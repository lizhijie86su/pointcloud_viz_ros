clc
clear 
tic;

for z=1 : 1
    snap()
    pause(6);
    pic_rename(z)

% folder = 'pic_col';
% for i=1:15
%     fn = sprintf('%s/Z%d/sc130/%d.png',folder,z,i);
%     imgs{i} = imread(fn);
% end

% [uphi, mod] = img2phase(imgs,8);
% figure;
%   idisp(uphi)

calibsession = load('stereoParams.mat');
ROWS =  1024;
COLS =  1280;
load('coeffs','coeffs','Zmin','Zmax')
load('undis.mat','undisL');
Lundisu = reshape(undisL(:,1),[ROWS, COLS]);
Lundisv = reshape(undisL(:,2),[ROWS, COLS]);
% % % % % % % % % % FPGA % % % % % % % % % % 
% for i=1:15
%     fn = sprintf('pic_col/Z%d/sc130/%d.png',z,i);
%     Isl{i} = imread(fn);
%     if(size(Isl{i},1)==1025)
%         Isl{i} = Isl{i}(1:end-1,1:end-1);
%     end
% end
% [ph, modl] = img2phase(Isl,8);
fn = sprintf('pic_col/Z%d/sc130/1.png',z);
ph = imread(fn);
ph = double(ph);
ph = (ph./256)*2*pi;

figure;
  idisp(ph)
% % % % % % % % % % % % % % % % % % % % % % 

Nfit = 3;
H=0;
for k=1:Nfit
    H  =  H  + coeffs(:,:,k).*ph.^(Nfit-k);
end
% H = gaussian_filter(H,3,3);
% 
H = med_filter( H, 3 ); 

% H = bfilter2(H,5,3, 0.1);
H = u_floor_filter(H,7,30); 

% H =  erode_pic (H,2);
% H = med_filter( H, 3 ); 
% H = med_filter( H, 3 );

% H =  avg_filter(H,3);   
% H = sobel_detector(H,2);
% Rob = Robert_Edge_Detector(H);
% H = H + Rob;
% H(modl<0.06)=NaN;
H(H<400)=NaN;
H(H>800)=NaN;

  cx =  calibsession.stereoParams.CameraParameters1.K(1,3);
  cy =  calibsession.stereoParams.CameraParameters1.K(2,3);
  fx = calibsession.stereoParams.CameraParameters1.K(1,1);
  fy = calibsession.stereoParams.CameraParameters1.K(2,2); 
 x = (Lundisu-cx)/fx;
 y = (Lundisv-cy)/fy;

X= x.*H;
Y = y.*H; 
figure
pcshow(pointCloud([X(:),Y(:),H(:)]))

end