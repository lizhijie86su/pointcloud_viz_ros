function [ img ] = med_filter( image, m )
%----------------------------------------------
%中值滤波
%输入：
%image：原图
%m：模板的大小3*3的模板，m=3
 
%输出：
%img：中值滤波处理后的图像
%----------------------------------------------
    n = m;
    [ height, width ] = size(image);
    x1 = image;
    x2 = x1;
    for i = 1: height-n+1
        for j = 1:width-n+1
            mb = x1( i:(i+n-1),  j:(j+n-1) );%获取图像中n*n的矩阵
            mb = mb(:);%将mb变成向量化，变成一个列向量
            mm = median(mb);%取中间值
            x2( i+(n-1)/2,  j+(n-1)/2 ) = mm;
 
        end
    end
 
    img = x2;
end


