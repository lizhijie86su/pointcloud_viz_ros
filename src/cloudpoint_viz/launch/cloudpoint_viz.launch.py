import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    rviz_config_dir = os.path.join(get_package_share_directory(
        'cloudpoint_viz'), 'config', 'config.rviz')
    assert os.path.exists(rviz_config_dir)

    viz_config_dir = os.path.join(get_package_share_directory(
        'cloudpoint_viz'), 'config', 'viz_config.json')

    resource_dir = os.path.join(get_package_share_directory(
        'cloudpoint_viz'), 'resource', 'viz')

    return LaunchDescription([
        Node(package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_dir],
            output='screen'
        ),
        Node(package='cloudpoint_viz', # defined in setup.py
            executable='cloudpoint_viz_subscriber_node', # defined in setup.py
            name='cloudpoint_viz',
            output='screen',
            arguments=[viz_config_dir, resource_dir],
        ),
    ])

