
import sys
import os
import json
import socket
import threading

import rclpy 
from rclpy.node import Node
import sensor_msgs.msg as sensor_msgs
import std_msgs.msg as std_msgs

import numpy as np
import open3d as o3d
from scipy.ndimage import median_filter
from scipy.signal import convolve2d

def u_floor_filter(img, window_size, threshold):
    window = np.ones((window_size, window_size)) / (window_size**2)  # Define the averaging window
    local_mean = convolve2d(img, window, mode='same')  # Calculate local mean using convolution
    local_squared_mean = convolve2d(img**2, window, mode='same')  # Calculate local squared mean using convolution
    local_variance = local_squared_mean - local_mean**2  # Calculate local variance
    return np.where(local_variance < np.full(img.shape, threshold, dtype=np.float32), img, 0.)


def middle_filter(image, kernel_size):
    filtered_image = median_filter(image, size=kernel_size)
    return filtered_image


class FrameData(object):
    def __init__(self, index, data):
        self.index = index
        self.data = data

class DataReceiver(object):
    FRAME_POOL_SIZE = 10
    UDP_PAYLOAD_SIZE = 8
    def __init__(self, ip, port, width, height, bit_width):
        self.ip = ip
        self.port = port
        self.width = width
        self.height = height
        self.bit_width = bit_width
        self.ready_data_pool = list()
        self.dtype = np.uint8 if self.bit_width == 8 else np.uint16
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 65536) 
        self.sock.settimeout(2)
        self.sock.bind((self.ip, self.port))
        self.th = threading.Thread(target=self.worker)
        self.th.daemon = True
        self.th.start()

    def get_one_frame(self):
        im_array = np.zeros((self.height,self.width), self.dtype)
        line_cnt = 0
        raw_data_line_size = (int)(self.UDP_PAYLOAD_SIZE + self.width * self.bit_width / 8)
        # print("try get one frame.", flush=True)
        while True:
            try:
                data = self.sock.recv(raw_data_line_size)
                line_index = data[5] + (data[4] << 8)
                # print("get line %d" % line_index, flush=True)
                if line_index == line_cnt:
                    dt = np.dtype(np.int16)
                    dt = dt.newbyteorder('>')
                    im_array[line_cnt, 0:self.width] = np.frombuffer(data, dtype=dt, offset=self.UDP_PAYLOAD_SIZE)
                    line_cnt += 1
                else:
                    line_cnt = 0
                    continue
            except Exception as e:
                print("Exception recv:", str(e))
                continue

            if line_cnt == self.height:
                break
        return im_array

    def worker(self):
        frame_counter = 0
        print("start udp receving worker.", flush=True)
        while True:
            frame = self.get_one_frame()
            frame_data = FrameData(frame_counter, frame)
            self.ready_data_pool.append(frame_data)
            frame_counter += 1
            print("get one frame: %d" % frame_counter, flush=True)
            if len(self.ready_data_pool) > self.FRAME_POOL_SIZE:
                print("pop one frame since frame pool is full. size = %s" % (len(self.ready_data_pool)))
                self.ready_data_pool.pop(0)

    def get(self):
        return self.ready_data_pool.pop(0) if len(self.ready_data_pool) > 0 else None


class CloudPointVizPublisher(Node):
    COLS = 1280
    ROWS = 1024
    def __init__(self):
        super().__init__('cloudpoint_viz_publisher_node')
        self.config_file = sys.argv[1]
        self.resource_dir = sys.argv[2]
        config = json.load(open(self.config_file))
        coeffs1_path = os.path.join(self.resource_dir, config["coeffs1"])
        coeffs2_path = os.path.join(self.resource_dir, config["coeffs2"])
        coeffs3_path = os.path.join(self.resource_dir, config["coeffs3"])
        su_path = os.path.join(self.resource_dir, config["su"])
        sv_path = os.path.join(self.resource_dir, config["sv"])

        data_path = os.path.join(self.resource_dir, config["debug_data"])

        self.cx = config["cx"]
        self.cy = config["cy"]
        self.fx = config["fx"]
        self.fy = config["fy"]
        self.bit_width = config["bit_width"]

        self.ip = config["ip"]
        self.port = config["port"]

        print("load coeffs1 from: %s" % coeffs1_path)
        print("load coeffs2 from: %s" % coeffs2_path)
        print("load coeffs3 from: %s" % coeffs3_path)
        print("load su from: %s" % su_path)
        print("load sv from: %s" % sv_path)
        print("cx = %f, cy = %f, fx = %f, fy = %f" % (self.cx, self.cy, self.fx, self.fy))
        print("device: %s:%d" % (self.ip, self.port))
        print("bit_width = %d" % self.bit_width)
        print("", flush=True)

        self.coeffs1 = self.read_param_bin(coeffs1_path).astype(np.float32)
        self.coeffs2 = self.read_param_bin(coeffs2_path).astype(np.float32)
        self.coeffs3 = self.read_param_bin(coeffs3_path).astype(np.float32)
        self.su = self.read_param_bin(su_path).astype(np.float32)
        self.sv = self.read_param_bin(sv_path).astype(np.float32)

        self.X_param = (self.su - self.cx) / self.fx
        self.Y_param = (self.sv - self.cy) / self.fy

        self.debug = config["debug"]
        if self.debug:
            self.test_data = self.read_data_bin(data_path, dtype=np.uint16 if self.bit_width == 16 else np.uint8).astype(np.float32)
        else:
            self.data_receiver = DataReceiver(self.ip, self.port, self.COLS, self.ROWS, self.bit_width)

        # I create a publisher that publishes sensor_msgs.PointCloud2 to the 
        # topic 'pcd'. The value '10' refers to the history_depth, which I 
        # believe is related to the ROS1 concept of queue size. 
        # Read more here: 
        # http://wiki.ros.org/rospy/Overview/Publishers%20and%20Subscribers
        self.viz_publisher = self.create_publisher(sensor_msgs.PointCloud2, 'pcd', 10)
        timer_period = 1/10.0
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        # Here I use the point_cloud() function to convert the numpy array 
        # into a sensor_msgs.PointCloud2 object. The second argument is the 
        # name of the frame the point cloud will be represented in. The default
        # (fixed) frame in RViz is called 'map'
        raw_data = None
        if self.debug:
            raw_data = self.test_data
        else:
            frame_data = self.data_receiver.get()
            if frame_data is not None:
                raw_data = frame_data.data
        if raw_data is not None:
            raw_data.reshape(self.ROWS, self.COLS)
            points = self.calc_data_cloud(raw_data)
            pcd = self.to_point_cloud(points, 'map')
            self.viz_publisher.publish(pcd)

    def read_data_bin(self, path, dtype=np.float64):
        return np.fromfile(path, dtype).reshape(self.ROWS, self.COLS)

    def read_param_bin(self, path, dtype=np.float64):
        return np.fromfile(path, dtype).reshape(self.COLS, self.ROWS).transpose()

    def calc_data_cloud(self, raw_data):
        P = raw_data * 2.0 * 3.1416 / 256.0
        H = self.coeffs1 * np.square(P)+ self.coeffs2 * P + self.coeffs3
        H = middle_filter(H, 3)
        H = u_floor_filter(H, 7, 30)
        H[(H > 800.0) | (H < 400.0)] = np.nan
        X = self.X_param * H
        Y = self.Y_param * H
        return np.stack((X, Y, H), axis=2)

    def to_point_cloud(self, points, parent_frame):
        """ Creates a point cloud message.
        Args:
            points: Frame-> [Row->[Point->[x,y,z],Point->[x,y,z],...]].
            parent_frame: frame in which the point cloud is defined
        Returns:
            sensor_msgs/PointCloud2 message

        Code source:
            https://gist.github.com/pgorczak/5c717baa44479fa064eb8d33ea4587e0

        References:
            http://docs.ros.org/melodic/api/sensor_msgs/html/msg/PointCloud2.html
            http://docs.ros.org/melodic/api/sensor_msgs/html/msg/PointField.html
            http://docs.ros.org/melodic/api/std_msgs/html/msg/Header.html

        """
        # In a PointCloud2 message, the point cloud is stored as an byte 
        # array. In order to unpack it, we also include some parameters 
        # which desribes the size of each individual point.
        ros_dtype = sensor_msgs.PointField.FLOAT32
        dtype = np.float32
        itemsize = np.dtype(dtype).itemsize # A 32-bit float takes 4 bytes.

        data = points.astype(dtype).tobytes() 

        # The fields specify what the bytes represents. The first 4 bytes 
        # represents the x-coordinate, the next 4 the y-coordinate, etc.
        fields = [sensor_msgs.PointField(
            name=n, offset=i*itemsize, datatype=ros_dtype, count=1)
            for i, n in enumerate('xyz')]

        # The PointCloud2 message also has a header which specifies which 
        # coordinate frame it is represented in. 
        header = std_msgs.Header(frame_id=parent_frame)

        return sensor_msgs.PointCloud2(
            header=header,
            height=points.shape[0], # 1024
            width=points.shape[1],  # 1280
            is_dense=False,
            is_bigendian=False,
            fields=fields,
            point_step=(itemsize * points.shape[2]), # Every point consists of three float32s.
            row_step=(itemsize * points.shape[2] * points.shape[1]),
            data=data
        )

def main(args=None):
    rclpy.init(args=args)
    viz_publisher = CloudPointVizPublisher()
    rclpy.spin(viz_publisher)
    
    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    viz_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
