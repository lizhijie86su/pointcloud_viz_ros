# pointcloud_viz_ros

## Install Ros2 on Windows
* http://docs.ros.org/en/foxy/Installation/Windows-Install-Binary.html
* https://zhuanlan.zhihu.com/p/414874250

## Build and Run Steps
使用管理员权限打开“x64 Native Tools Command Prompt for VS 2019”，执行如下命令 （VS2022也可以）：
* colcon build --symlink-install --packages-select cloudpoint_viz
* install\setup.bat
* ros2 launch cloudpoint_viz cloudpoint_viz.launch.py

## Packages
* pip3 install open3d
* pip3 install scipy

## Config File
src/config/viz_config.json

```
{
    "device_ip" : "127.0.0.1",
    "device_port" : 5000,
    "bit_width": 8,
    "coeffs1": "coeffs1.bin",
    "coeffs2": "coeffs2.bin",
    "coeffs3": "coeffs3.bin",
    "su": "LundiSU.bin",
    "sv": "LundiSV.bin",
    "cx": 678.7566,
    "cy": 527.2745,
    "fx": 1.5686e+03,
    "fy": 1.5689e+03,
    "debug": false,
    "debug_data": "pic_8bit.bin"
}
```
